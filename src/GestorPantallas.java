import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class GestorPantallas {
	protected static Display display;
	protected static Stack pilaPantallas;
	protected static Pantalla pantallaActual;
	public static PantallaProgramas pantallaProgramas;
	public static PantallaCodigo pantallaCodigo;
	public static PantallaDescripcion pantallaDescripcion;
	public static PantallaSalida pantallaSalida;
	public static PantallaSalidaErrores pantallaSalidaErrores;
	public static PantallaEntrada pantallaEntrada;
	public static PantallaGuardarComo pantallaGuardarComo;
	
	static {
		pilaPantallas = new Stack();
		pantallaActual = null;
		pantallaProgramas = new PantallaProgramas();
		pantallaCodigo = new PantallaCodigo();
		pantallaDescripcion = new PantallaDescripcion();
		pantallaSalida = new PantallaSalida();
		pantallaSalidaErrores = new PantallaSalidaErrores();
		pantallaEntrada = new PantallaEntrada();
		pantallaGuardarComo = new PantallaGuardarComo();
	}
	
	public static void ponDisplay(MIDlet m) {
		display = Display.getDisplay(m);
	}
	
	public static void abrirPantalla(Pantalla p) {
		pilaPantallas.push(pantallaActual);
		pantallaActual = p;
		p.alAbrirPantalla();
		display.setCurrent((Displayable)p);
		if (p instanceof Runnable)
			new Thread((Runnable)p).start();
	}
	
	public static void cerrarPantalla() {
		if (pilaPantallas.empty()) {
		} else {
			pantallaActual.alCerrarPantalla();
			pantallaActual = (Pantalla)pilaPantallas.pop();
			if (pantallaActual != null) {
				pantallaActual.alAbrirPantalla();
				display.setCurrent((Displayable)pantallaActual);
			} else
				MiMIDlet.finalizarAplicacion();
		}
	}
}