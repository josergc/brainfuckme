import javax.microedition.lcdui.*;

public class PantallaSalidaErrores extends TextBox implements Pantalla, CommandListener {
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);

	public PantallaSalidaErrores() {
		super("Errores",null,32 * 1024,TextField.ANY);
		
		addCommand(cmdVolver);
		setCommandListener(this);
	}

	public void commandAction(Command c, Displayable d) {
		if (c == cmdVolver) {
			GestorPantallas.cerrarPantalla();
		}
	}
	
	public void alAbrirPantalla() {
	}
	
	public void alCerrarPantalla() {
	}

}