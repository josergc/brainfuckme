/*

Int�rprete de Brainfuck

Autor: Jos� Roberto Garc�a Chico
Versi�n: 1.0
Fecha: 17 de junio de 2006
Web: www.josergc.tk
e-mail: josergc@lycos.es

*/

public class Brainfuck {

	protected final char MAYOR = '>';
	protected final char MENOR = '<';
	protected final char MAS = '+';
	protected final char MENOS = '-';
	protected final char PUNTO = '.';
	protected final char COMA = ',';
	protected final char ABRIR_CORCHETE = '[';
	protected final char CERRAR_CORCHETE = ']';
	protected final char SALTO_LINEA = '\n';
	
	protected char[] programa;
	protected char[] memoria;
	protected char[] entrada;
	protected char salida;
	protected int ptr;
	protected int ptrEntrada;
	protected int nCorchetesAbiertos;
	protected java.util.Stack pilaEjecucion;
	
	public Brainfuck() {
		programa = new char[0];
		memoria = new char[32 * 1024];
		entrada = new char[0];
		salida = ' ';
		pilaEjecucion = new java.util.Stack();
	}
	
	public void ponPrograma(String programa) {
		this.programa = programa.toCharArray();
	}
	
	public void ponEntrada(String entrada) {
		this.entrada = this.entrada.length - ptrEntrada> 0 ? (new String(this.entrada,ptrEntrada,this.entrada.length) + entrada).toCharArray() : entrada.toCharArray();
		ptrEntrada = 0;
	}
	
	public char devSalida() {
		return salida;
	}

	protected InformacionInterprete ii;
	
	public void inicia() {
		for (int i = 0; i < memoria.length; i++)
			memoria[0] = '\0';
		ptr = 0;
		ptrEntrada = 0;
		pilaEjecucion.removeAllElements();
		ii = new InformacionInterprete();
	}
	
	protected InformacionInterprete ejecuta() {
		if (ii.pc < programa.length) {
			ii.mensaje = ii.MSJ_CONTINUA;
			switch(programa[ii.pc++]) {
				case MAYOR:
					++ptr;
					ii.columna++;
				break;
				case MENOR:
					--ptr;
					ii.columna++;
				break;
				case MAS:
					++memoria[ptr];
					ii.columna++;
				break;
				case MENOS:
					--memoria[ptr];
					ii.columna++;
				break;
				case PUNTO:
					salida = memoria[ptr];
					ii.columna++;
					ii.mensaje = ii.MSJ_SALIDA_DATOS;
				break;
				case COMA:
					if (ptrEntrada >= entrada.length) {
						ii.pc--;
						ii.mensaje = ii.MSJ_NECESITA_ENTRADA;
						return ii;
					} else {
						memoria[ptr] = entrada[ptrEntrada++];
						ii.columna++;
					}
				break;
				case ABRIR_CORCHETE:
					if (memoria[ptr] == 0) {
						// Adelanta el contador del programa hasta el final del corchete
						nCorchetesAbiertos = 1;
						try {
							while (nCorchetesAbiertos > 0)
								switch(programa[ii.pc++]) {
									case ABRIR_CORCHETE:
										nCorchetesAbiertos++;
										ii.columna++;
									break;
									case CERRAR_CORCHETE:
										nCorchetesAbiertos--;
										ii.columna++;
									break;
									case SALTO_LINEA:
										ii.columna = 1;
										ii.fila++;
									break;
								}
						} catch (ArrayIndexOutOfBoundsException e) {
							ii.mensaje = ii.MSJ_ERROR_CORCHETE_SIN_CERRAR;
						}
					} else {
						ii.pc--;
						pilaEjecucion.push(ii);
						ii = new InformacionInterprete(ii);
						ii.pc++;
						ii.columna++;
					}
				break;
				case CERRAR_CORCHETE:
					ii = (InformacionInterprete)pilaEjecucion.pop();
				break;
				case SALTO_LINEA:
					ii.columna = 1;
					ii.fila++;
				break;
			}
		} else
			ii.mensaje = ii.MSJ_FINALIZADO;
		return ii;
	}
}