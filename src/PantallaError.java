import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaError extends Form implements CommandListener, Pantalla {
	protected static Image imgError;
	
	static {
		try {
			imgError = Image.createImage("/error.png");
		} catch (IOException e) {
			imgError = null;
		}
	}
	
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);
	
	public PantallaError(String mensaje) {
		super("Error en la aplicación");
		inicia("Mensaje de error",mensaje);
	}
	
	public PantallaError(String titulo, String mensaje) {
		super("Error en la aplicación");
		inicia(titulo,mensaje);
	}
	
	protected void inicia(String titulo, String mensaje) {
		append(new ImageItem("",imgError,ImageItem.LAYOUT_CENTER,""));
		append(new StringItem(titulo,mensaje));
		addCommand(cmdVolver);
		setCommandListener(this);
	}
	
	public void commandAction(Command c, Displayable d) {
		GestorPantallas.cerrarPantalla();
	}
	
	public void alAbrirPantalla() {
	}
	
	public void alCerrarPantalla() {
	}
}