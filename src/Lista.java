import javax.microedition.lcdui.*;

public class Lista extends List {
	public Lista(String title, int listType) {
		super(title,listType);
	}
	public Lista(String title, int listType, String[] stringElements, Image[] imageElements) {
		super(title,listType,stringElements,imageElements);
	}
	public void eliminaElementosLista() {
		// Este m�todo est� implementado porque no funciona el m�todo "deleteAll"
		while (size() > 0)
			delete(0);
	}	
}