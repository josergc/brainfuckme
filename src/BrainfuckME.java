import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class BrainfuckME extends MiMIDlet {
	protected void startApp() throws 
		MIDletStateChangeException
		{
		super.startApp();
		GestorPantallas.ponDisplay(this);
		GestorPantallas.abrirPantalla(GestorPantallas.pantallaProgramas);
	}
	protected void pauseApp() {
		super.pauseApp();
	}
	protected void destroyApp(boolean unconditional) throws 
		MIDletStateChangeException
		{
		super.destroyApp(unconditional);
	}
}