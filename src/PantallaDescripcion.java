import javax.microedition.lcdui.*;

public class PantallaDescripcion extends TextBox implements Pantalla, CommandListener {
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);
	
	protected boolean pantallaActivada;

	public PantallaDescripcion() {
		super(null,null,1024,TextField.ANY);
		
		addCommand(cmdVolver);
		setCommandListener(this);
		
		pantallaActivada = false;
	}
	
	public void ponDescripcion(String descripcion) {
		setString(descripcion);
	}
	
	public String devDescripcion() {
		return getString();
	}
	
	public void commandAction(Command c, Displayable d) {
		if (!pantallaActivada)
			return;
		if (c == cmdVolver) {
			GestorPantallas.cerrarPantalla();
		}
	}
	
	public void alAbrirPantalla() {
		pantallaActivada = true;
	}
	
	public void alCerrarPantalla() {
		pantallaActivada = false;
	}
}