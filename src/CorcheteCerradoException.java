public class CorcheteCerradoException extends Exception {
	public CorcheteCerradoException(String mensaje) {
		super(mensaje);
	}
}