import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaMensajeSiNo extends Form implements CommandListener, Pantalla {
	protected static Image imgMensaje;
	
	static {
		try {
			imgMensaje = Image.createImage("/mensaje.png");
		} catch (IOException e) {
			imgMensaje = null;
		}
	}
	
	public final static int CMD_SI = 0;
	public final static int CMD_NO = 1;
	
	protected final static Command cmdSi = new Command("S�",Command.OK,1);
	protected final static Command cmdNo = new Command("No",Command.CANCEL,1);
	protected int ultimoComando;
	
	public PantallaMensajeSiNo(String mensaje) {
		super("Mensaje");
		inicia("Mensaje",mensaje);
	}
	
	public PantallaMensajeSiNo(String titulo, String mensaje) {
		super("Mensaje");
		inicia(titulo,mensaje);
	}
	
	protected void inicia(String titulo, String mensaje) {
		append(new ImageItem("",imgMensaje,ImageItem.LAYOUT_CENTER,""));
		append(new StringItem(titulo,mensaje));
		addCommand(cmdSi);
		addCommand(cmdNo);
		setCommandListener(this);
	}
	
	public void commandAction(Command c, Displayable d) {
		if (c == cmdSi) {
			ultimoComando = CMD_SI;
		} else
			ultimoComando = CMD_NO;
		GestorPantallas.cerrarPantalla();
	}
	
	public void alAbrirPantalla() {
	}
	
	public void alCerrarPantalla() {
	}
	
	public int devUltimoComando() {
		return ultimoComando;
	}
}