import java.io.*;
import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.rms.*;

public class PantallaProgramas extends Lista implements Pantalla, CommandListener {
	public final static int CMD_NUEVO = 0;
	public final static int CMD_ABRIR = 1;
	public final static int CMD_VER_DESCRIPCION = 2;
	public final static int CMD_ELIMINAR = 3;
	public final static int CMD_BRAINFUCK = 4;
	public final static int CMD_ACERCA_DE = 5;
	public final static int CMD_DONATIVO = 6;
	public final static int CMD_SALIR = 7;

	protected static int nProgramasCreados;
	
	protected int ultimoComando;
	
	protected static Image imgSinAbrir;
	protected static Image imgAbierto;
	protected static Image imgSinGrabar;
	
	protected PantallaMensajeSiNo pantallaMensajeSiNo;
	
	static {
		nProgramasCreados = 0;
		// Carga las imagenes
		try {
			imgSinAbrir = Image.createImage("/sinAbrir.png");
			imgAbierto = Image.createImage("/abierto.png");
			imgSinGrabar = Image.createImage("/sinGrabar.png");
		} catch (IOException e) {
			imgSinAbrir = null;
			imgAbierto = null;
			imgSinGrabar = null;
		}
	}

	protected boolean pantallaActivada;
	
	protected final static Command cmdNuevo = new Command("Nuevo",Command.ITEM,1);
	protected final static Command cmdAbrir = new Command("Abrir",Command.ITEM,1);
	protected final static Command cmdVerDescripcion = new Command("Ver descripci�n",Command.ITEM,1);
	protected final static Command cmdEliminar = new Command("Eliminar",Command.ITEM,1);
	protected final static Command cmdBrainfuck = new Command("Brainfuck",Command.ITEM,1);
	protected final static Command cmdAcercaDe = new Command("Acerca de...",Command.ITEM,1);
	protected final static Command cmdDonativo = new Command("Haz un donativo",Command.ITEM,1);
	protected final static Command cmdSalir = new Command("Salir",Command.ITEM,1);

	protected Hashtable programasAbiertos;
	
	public PantallaProgramas() {
		super("Programas",Lista.IMPLICIT);
		
		programasAbiertos = new Hashtable();
		
		addCommand(cmdNuevo);
		addCommand(cmdAbrir);
		addCommand(cmdVerDescripcion);
		addCommand(cmdEliminar);
		addCommand(cmdBrainfuck);
		addCommand(cmdAcercaDe);
		addCommand(cmdDonativo);
		addCommand(cmdSalir);
		setCommandListener(this);
		
		pantallaActivada = false;
		ultimoComando = CMD_SALIR;
	}
	
	public void commandAction(Command c, Displayable d) {
		if (!pantallaActivada) 
			return;
		if (c == cmdNuevo) {
			ultimoComando = CMD_NUEVO;
			Programa p = new Programa();
			p.ponNombre("p" + (++nProgramasCreados));
			programasAbiertos.put(
				p.devNombre(),
				p
				);
			GestorPantallas.pantallaCodigo.ponPrograma(p);
			GestorPantallas.abrirPantalla(GestorPantallas.pantallaCodigo);
		} else if (c == cmdAbrir || c == SELECT_COMMAND) {
			ultimoComando = CMD_ABRIR;
			if (getSelectedIndex() != -1) 
				try {
					Programa p;
					programasAbiertos.put(
						getString(getSelectedIndex()),
						p = Programa.carga(getString(getSelectedIndex()))
						);
					GestorPantallas.pantallaCodigo.ponPrograma(p);
					GestorPantallas.abrirPantalla(GestorPantallas.pantallaCodigo);
				} catch(RecordStoreNotOpenException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Tabla no abierta",e.toString())
						);
				} catch(InvalidRecordIDException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("ID no v�lida",e.toString())
						);
				} catch(IOException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Error de entrada/salida",e.toString())
						);
				} catch(RecordStoreException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Error en la tabla",e.toString())
						);
				}
		} else if (c == cmdVerDescripcion) {
			ultimoComando = CMD_VER_DESCRIPCION;
			if (getSelectedIndex() != -1) 
				try {
					GestorPantallas.pantallaDescripcion.ponDescripcion(
						Programa.devDescripcion(getString(getSelectedIndex()))
						);
					GestorPantallas.abrirPantalla(
						GestorPantallas.pantallaDescripcion
						);
				} catch(RecordStoreNotOpenException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Tabla no abierta",e.toString())
						);
				} catch(InvalidRecordIDException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("ID no v�lida",e.toString())
						);
				} catch(IOException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Error de entrada/salida",e.toString())
						);
				} catch(RecordStoreException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Error en la tabla",e.toString())
						);
				}
		} else if (c == cmdEliminar) {
			ultimoComando = CMD_ELIMINAR;
			if (getSelectedIndex() != -1) 
				GestorPantallas.abrirPantalla(
					pantallaMensajeSiNo = new PantallaMensajeSiNo(
						"Va a eliminar el programa " + getString(getSelectedIndex()),
						"�Continuar?"
						)
					);
		} else if (c == cmdBrainfuck) {
			ultimoComando = CMD_BRAINFUCK;
			GestorPantallas.abrirPantalla(new PantallaBrainfuck());
		} else if (c == cmdAcercaDe) {
			ultimoComando = CMD_ACERCA_DE;
			GestorPantallas.abrirPantalla(new PantallaAcercaDe());
		} else if (c == cmdDonativo) {
			ultimoComando = CMD_DONATIVO;
			GestorPantallas.abrirPantalla(new PantallaHacerDonativo());
		} else if (c == cmdSalir) {
			ultimoComando = CMD_SALIR;
			GestorPantallas.cerrarPantalla();
		}
	}
	
	public int devUltimoComando() {
		return ultimoComando;
	}

	public void actualizarLista() {
		Enumeration listaProgramas;
		try {
			listaProgramas = Programa.devListaProgramas();
		} catch(RecordStoreNotOpenException e) {
			GestorPantallas.abrirPantalla(
				new PantallaError("Tabla no abierta",e.toString())
				);
			return;
		} catch(InvalidRecordIDException e) {
			GestorPantallas.abrirPantalla(
				new PantallaError("ID no v�lida",e.toString())
				);
			return;
		} catch(IOException e) {
			GestorPantallas.abrirPantalla(
				new PantallaError("Error de entrada/salida",e.toString())
				);
			return;
		} catch(RecordStoreException e) {
			GestorPantallas.abrirPantalla(
				new PantallaError("Error en la tabla",e.toString())
				);
			return;
		}
		eliminaElementosLista();
		
		if (listaProgramas.hasMoreElements()) {
			Image img;
			Programa p;
			String nombre;
			Vector programasEnLista = new Vector();
			while (listaProgramas.hasMoreElements()) {
				programasEnLista.addElement(nombre = (String)listaProgramas.nextElement());
				p = (Programa)programasAbiertos.get(nombre);
				img = (p == null) ? imgSinAbrir : (p.haSidoModificado() ? imgSinGrabar : imgAbierto);
				append(nombre,img);
			}
			listaProgramas = programasAbiertos.elements();
			while (listaProgramas.hasMoreElements()) {
				p = (Programa)listaProgramas.nextElement();
				if (!programasEnLista.contains(p.devNombre())) 
					append(p.devNombre(),imgSinGrabar);
			}
		} else {
			// Introducimos algunos de ejemplo como el "hola mundo"
			Programa p;
			p = new Programa();
			p.ponNombre("hello,world");
			p.ponDescripcion("T�pico programa que imprime por pantalla el mensaje 'hello,world'");
			p.ponCodigo("++++++++++[>++++++++++<-]>++++.---.+++++++..+++.>++++[>+++++++++++<-]>.------------.[-]<<++++++++.--------.+++.------.--------.[-]<+[>++++++++++<-]>.[-]<");
			try {
				Programa.guarda(p);
				append(p.devNombre(),imgSinAbrir);
			} catch(Exception e) {
			}
		}
	}
	
	public void alAbrirPantalla() {
		pantallaActivada = true;
		switch(ultimoComando) {
			case CMD_NUEVO:
			break;
			case CMD_ABRIR:
				switch(GestorPantallas.pantallaCodigo.devUltimoComando()) {
					case PantallaCodigo.CMD_VOLVER:
						Programa p = GestorPantallas.pantallaCodigo.devProgramaActual();
					break;
					case PantallaCodigo.CMD_CERRAR:
						
					break;
				}
			break;
			case CMD_ELIMINAR:
				if (pantallaMensajeSiNo.devUltimoComando() == PantallaMensajeSiNo.CMD_SI) 
					try {
						Programa.elimina(getString(getSelectedIndex()));
					} catch(RecordStoreNotOpenException e) {
						GestorPantallas.abrirPantalla(
							new PantallaError("Tabla no abierta",e.toString())
							);
					} catch(InvalidRecordIDException e) {
						GestorPantallas.abrirPantalla(
							new PantallaError("ID no v�lida",e.toString())
							);
					} catch(IOException e) {
						GestorPantallas.abrirPantalla(
							new PantallaError("Error de entrada/salida",e.toString())
							);
					} catch(RecordStoreException e) {
						GestorPantallas.abrirPantalla(
							new PantallaError("Error en la tabla",e.toString())
							);
					}
				pantallaMensajeSiNo = null;
			break;
			case CMD_SALIR:
		}
		actualizarLista();
	}
	
	public void alCerrarPantalla() {
		pantallaActivada = false;
	}

}