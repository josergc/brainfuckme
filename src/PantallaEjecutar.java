import javax.microedition.lcdui.*;

public class PantallaEjecutar extends Form implements Pantalla, CommandListener, Runnable {
	public final static int CMD_PAUSAR_EJECUCION = 0;
	public final static int CMD_CONTINUAR_EJECUCION = 1;
	public final static int CMD_DETENER_EJECUCION = 2;
	public final static int CMD_VOLVER = 3;

	protected final static Command cmdAceptarEntradaDatos = new Command("Aceptar datos",Command.OK,1);
	protected final static Command cmdPausarEjecucion = new Command("Pausar ejecución",Command.ITEM,1);
	protected final static Command cmdContinuarEjecucion = new Command("Continuar ejecución",Command.ITEM,1);
	protected final static Command cmdDetenerEjecucion = new Command("Detener ejecución",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);
	
	protected final static TextField datosEntrada = new TextField(null,"",32 * 1024,TextField.ANY);
	protected boolean pidiendoDatosEntrada;
	
	protected Brainfuck brainfuck;
	protected boolean ejecutando;
	protected boolean noPausado;
	protected int ultimoComando;
	protected InformacionInterprete ii;

	
	public PantallaEjecutar(Brainfuck brainfuck) {
		super("Ejecuntado...");
	
		this.brainfuck = brainfuck;
		
		addCommand(cmdAceptarEntradaDatos);
		addCommand(cmdPausarEjecucion);
		addCommand(cmdContinuarEjecucion);
		addCommand(cmdDetenerEjecucion);
		setCommandListener(this);
		
		ejecutando = true;
		noPausado = true;
		pidiendoDatosEntrada = false;
		
		brainfuck.inicia();
	}
	
	public void run() {
		char c;
		StringBuffer sb = new StringBuffer();
		do {
			ii = brainfuck.ejecuta();
			switch (ii.mensaje) {
				case InformacionInterprete.MSJ_NECESITA_ENTRADA:
					// Muestra la cadena de texto hasta el momento y la casilla 
					// para la entrada de datos
					if (sb.length() > 0) {
						append(sb.toString());
						sb = new StringBuffer();
					}
					datosEntrada.setString("");
					append(datosEntrada);
					pidiendoDatosEntrada = true;
					return;
				case InformacionInterprete.MSJ_SALIDA_DATOS:
					// Muestra la cadena de texto si se llega a un retorno de 
					// carro
					c = brainfuck.devSalida();
					if (c == '\n') {
						append(sb.toString());
						sb = new StringBuffer();
					} else
						sb = sb.append(c);
				break;
				case InformacionInterprete.MSJ_ERROR:
					System.out.println("Error sin especificar " + ii.mensaje);
					return;
				case InformacionInterprete.MSJ_ERROR_CORCHETE_SIN_CERRAR:
					System.out.println("Error: corchete sin cerrar en la fila " + ii.fila + " columna " + ii.columna);
					return;
			}
		} while(ii.mensaje != ii.MSJ_FINALIZADO && noPausado);
		
		// Muestra la salida de datos si quedaba algo por mostrar
		if (!noPausado)
			if (sb.length() > 0)
				append(sb.toString());
		ejecutando = false;
		
		sb = new StringBuffer();
		for (int i = 0; i < size(); i++) 
			sb.append(((StringItem)get(i)).getText() + "\n");
		String cadena = sb.toString();
		GestorPantallas.pantallaSalida.setString(
			cadena.length() < GestorPantallas.pantallaSalida.getMaxSize() ? cadena : new String(cadena.toCharArray(),cadena.length() - GestorPantallas.pantallaSalida.getMaxSize(),cadena.length())
			);
		GestorPantallas.cerrarPantalla();
		GestorPantallas.abrirPantalla(GestorPantallas.pantallaSalida);
	}
	
	public int devUltimoComando() {
		return ultimoComando;
	}
	
	public void commandAction(Command c, Displayable d) {
		if (c == cmdAceptarEntradaDatos) {
			if (pidiendoDatosEntrada) {
				delete(size() - 1);
				brainfuck.ponEntrada(datosEntrada.getString());
				pidiendoDatosEntrada = false;
				new Thread(this).start();
			}
		} else if (c == cmdPausarEjecucion) {
			ultimoComando = CMD_PAUSAR_EJECUCION;
			noPausado = false;
		} else if (c == cmdContinuarEjecucion) {
			ultimoComando = CMD_CONTINUAR_EJECUCION;
			if (!ejecutando) {
				noPausado = true;
				new Thread(this).start();
			}
		} else if (c == cmdVolver) {
			ultimoComando = CMD_VOLVER;
			GestorPantallas.cerrarPantalla();
		}
	}
	
	public void alAbrirPantalla() {
	}
	
	public void alCerrarPantalla() {
	}
}