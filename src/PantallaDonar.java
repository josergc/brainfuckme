import javax.microedition.lcdui.*;

public class PantallaDonar extends Lista implements CommandListener, Pantalla {
	protected final static Command cmdSeleccionar = new Command("Enviar donativo",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);

	public PantallaDonar() {
		super("Selecciona pa�s",IMPLICIT);
		
		for (int i = 0; i < PaisDonativo.paises.length; i++)
			append(PaisDonativo.paises[i].pais,null);
		append("--== Env�o manual ==--",null);
		append("--== Otros ==--",null);
		
		addCommand(cmdSeleccionar);
		addCommand(cmdVolver);
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		if (c == cmdSeleccionar || c == SELECT_COMMAND) {
			if (getSelectedIndex() != -1) {
				if (getSelectedIndex() == PaisDonativo.paises.length) {
					GestorPantallas.abrirPantalla(
						new PantallaDonativoManual()
						);
				} else if (getSelectedIndex() == PaisDonativo.paises.length + 1) {
					GestorPantallas.abrirPantalla(
						new PantallaMensaje("Si quieres enviar un donativo y tu pa�s no sale en la lista, es posible que quieras ver si se ha incorporado despu�s de la publicaci�n de esta aplicaci�n entrando en www.josergc.tk y en el apartado de \'donativos\'.")
						);
				} else {
					// Env�a el mensaje
					GestorPantallas.abrirPantalla(
						new PantallaEnviarMensaje(
							PaisDonativo.paises[getSelectedIndex()].cadenaAEnviar,
							PaisDonativo.paises[getSelectedIndex()].telefono
							)
						);
				}
			}
		} else if (c == cmdVolver) {
			GestorPantallas.cerrarPantalla();
		}
	}
	public void alAbrirPantalla() {
	}
	public void alCerrarPantalla() {
	}
}