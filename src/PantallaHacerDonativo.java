/*
	Esta pantalla muestra los datos acerca del juego
*/
import javax.microedition.lcdui.*;

public class PantallaHacerDonativo extends Form implements CommandListener, Pantalla {
	protected final static Command cmdDonar = new Command("Donar",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);

	public PantallaHacerDonativo() {
		super("Haz un donativo");
		
		append(new StringItem("Haz un donativo","Si te ha gustado esta aplicaci�n, puedes hacer un donativo para animarme a realizar m�s proyectos."));
		append(new StringItem("�C�mo hacerlo?","Puedes realizar tu donativo de diversas maneras."));
		append(new StringItem("Mediante un micropago","Este m�todo consiste en el que la aplicaci�n enviar� un SMS a mi proveedor de micropagos (www.sepomo.com) previa selecci�n del pa�s desde donde vas a hacer el pago. Una vez enviado el SMS, deber�as recibir un SMS de respuesta con un c�digo. En mi p�gina web (www.josergc.tk) podr�s encontrar un enlace donde introducir el c�digo para que se me haga efectivo el donativo."));
		append(new StringItem("A trav�s de PayPal","Si dispones de una cuenta de PayPal, puedes hacer tu donativo a josergc@lycos.es"));
		append(new StringItem("Otros","Si quieres hacer cualquier otro tipo de donativo o mediante otra manera, ponte en contacto conmigo enviando un e-mail a josergc@lycos.es"));
		append(new StringItem("Ante todo","Darte las gracias por usar la aplicaci�n y molestarte en leer esto :-)"));
		append(new StringItem("El autor","Jos� Roberto Garc�a Chico"));
		
		addCommand(cmdDonar);
		addCommand(cmdVolver);
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		if (c == cmdDonar) {
			GestorPantallas.abrirPantalla(
				new PantallaDonar()
				);
		} else
			GestorPantallas.cerrarPantalla();
	}
	public void alAbrirPantalla() {
	}
	public void alCerrarPantalla() {
	}
}