import java.io.*;
import javax.microedition.lcdui.*;
import javax.microedition.rms.*;

public class PantallaCodigo extends TextBox implements Pantalla, CommandListener {
	public final static int CMD_EJECUTAR = 0;
	public final static int CMD_GUARDAR = 1;
	public final static int CMD_GUARDAR_COMO = 2;
	public final static int CMD_VER_DESCRIPCION = 3;
	public final static int CMD_CERRAR = 4;
	public final static int CMD_VOLVER = 5;
	
	protected int ultimoComando;

	protected final static Command cmdEjecutar = new Command("Ejecutar",Command.ITEM,1);
	protected final static Command cmdGuardar = new Command("Guardar",Command.ITEM,1);
	protected final static Command cmdGuardarComo = new Command("Guardar como...",Command.ITEM,1);
	protected final static Command cmdVerDescripcion = new Command("Ver descripci�n",Command.ITEM,1);
	protected final static Command cmdVerEntrada = new Command("Ver entrada",Command.ITEM,1);
	protected final static Command cmdVerSalida = new Command("Ver salida",Command.ITEM,1);
	protected final static Command cmdCerrar = new Command("Cerrar",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);
	
	protected Brainfuck brainfuck;
	protected Programa programaActual;
	
	protected boolean pantallaActivada;
	
	public PantallaCodigo() {
		super(null,null,32 * 1024,TextField.ANY);
		
		brainfuck = new Brainfuck();
		
		addCommand(cmdEjecutar);
		addCommand(cmdGuardar);
		addCommand(cmdGuardarComo);
		addCommand(cmdVerDescripcion);
		addCommand(cmdCerrar);
		addCommand(cmdVolver);
		setCommandListener(this);
		
		pantallaActivada = false;
		ultimoComando = CMD_VOLVER;
	}
	
	public void ponPrograma(Programa p) {
		String cadena;

		programaActual = p;
		
		cadena = p.devCodigo();
		brainfuck.ponPrograma(cadena);
		setString(cadena);
		
		cadena = p.devDescripcion();
		GestorPantallas.pantallaDescripcion.ponDescripcion(cadena);
	}
	
	public Programa devProgramaActual() {
		return programaActual;
	}
	
	public int devUltimoComando() {
		return ultimoComando;
	}

	public void commandAction(Command c, Displayable d) {
		if (!pantallaActivada)
			return;
		if (c == cmdEjecutar) {
			ultimoComando = CMD_EJECUTAR;
			brainfuck.ponPrograma(getString());
			GestorPantallas.abrirPantalla(
				new PantallaEjecutar(brainfuck)
				);
		} else if (c == cmdGuardar) {
			ultimoComando = CMD_GUARDAR;
			programaActual.ponCodigo(getString());
			programaActual.ponDescripcion(
				GestorPantallas.pantallaDescripcion.devDescripcion()
				);
			try {
				Programa.guarda(programaActual);
			} catch(RecordStoreNotOpenException e) {
				GestorPantallas.abrirPantalla(
					new PantallaError("Tabla no abierta",e.toString())
					);
			} catch(InvalidRecordIDException e) {
				GestorPantallas.abrirPantalla(
					new PantallaError("ID no v�lida",e.toString())
					);
			} catch(IOException e) {
				GestorPantallas.abrirPantalla(
					new PantallaError("Error de entrada/salida",e.toString())
					);
			} catch(RecordStoreException e) {
				GestorPantallas.abrirPantalla(
					new PantallaError("Error en la tabla",e.toString())
					);
			}
		} else if (c == cmdGuardarComo) {
			ultimoComando = CMD_GUARDAR_COMO;
		} else if (c == cmdVerDescripcion) {
			ultimoComando = CMD_VER_DESCRIPCION;
			GestorPantallas.abrirPantalla(GestorPantallas.pantallaDescripcion);
		} else if (c == cmdCerrar) {
			ultimoComando = CMD_CERRAR;
			GestorPantallas.cerrarPantalla();
		} else if (c == cmdVolver) {
			ultimoComando = CMD_VOLVER;
			if (getString().compareTo(programaActual.devCodigo()) != 0)
				programaActual.ponCodigo(getString());
			GestorPantallas.cerrarPantalla();
		}
	}
	
	public void alAbrirPantalla() {
		pantallaActivada = true;
		
		switch (ultimoComando) {
			case CMD_EJECUTAR:
				
			break;
			case CMD_VER_DESCRIPCION:
				if (GestorPantallas.pantallaDescripcion.devDescripcion().compareTo(programaActual.devDescripcion()) != 0)
					programaActual.ponDescripcion(
						GestorPantallas.pantallaDescripcion.devDescripcion()
						);
			break;
		} 
	}
	
	public void alCerrarPantalla() {
		pantallaActivada = false;
	}
}