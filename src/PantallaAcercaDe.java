/*
	Esta pantalla muestra los datos acerca del juego
*/
import javax.microedition.lcdui.*;

public class PantallaAcercaDe extends Form implements CommandListener, Pantalla {
	public PantallaAcercaDe() {
		super("Acerca de...");
		
		append(new StringItem("Aplicaci�n","Brainfuck mobile edition"));
		append(new StringItem("Versi�n","1.0"));
		append(new StringItem("Fecha","10 de junio de 2.006"));
		append(new StringItem("Autor","Jos� Roberto Garc�a Chico"));
		append(new StringItem("e-mail","josergc@lycos.es"));
		append(new StringItem("Web","www.josergc.tk"));
		append(new StringItem("Licencia","Esto es software libre seg�n la licencia de copyleft GNU/GPL versi�n 2"));
		
		addCommand(new Command("Volver",Command.EXIT,1));
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		GestorPantallas.cerrarPantalla();
	}
	public void alAbrirPantalla() {
	}
	public void alCerrarPantalla() {
	}
}