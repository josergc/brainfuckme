public class CorcheteSinCerrarException extends Exception {
	public CorcheteSinCerrarException(int fila, int columna) {
		super("Encontrado ] sin [ en fila " + fila + " columna " + columna);
	}
}