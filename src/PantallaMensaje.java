import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaMensaje extends Form implements CommandListener, Pantalla {
	protected static Image imgMensaje;
	
	static {
		try {
			imgMensaje = Image.createImage("/mensaje.png");
		} catch (IOException e) {
			imgMensaje = null;
		}
	}
	
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);
	
	public PantallaMensaje(String mensaje) {
		super("Mensaje");
		inicia("Mensaje",mensaje);
	}
	
	public PantallaMensaje(String titulo, String mensaje) {
		super("Mensaje");
		inicia(titulo,mensaje);
	}
	
	protected void inicia(String titulo, String mensaje) {
		append(new ImageItem("",imgMensaje,ImageItem.LAYOUT_CENTER,""));
		append(new StringItem(titulo,mensaje));
		addCommand(cmdVolver);
		setCommandListener(this);
	}
	
	public void commandAction(Command c, Displayable d) {
		GestorPantallas.cerrarPantalla();
	}
	
	public void alAbrirPantalla() {
	}
	
	public void alCerrarPantalla() {
	}
}