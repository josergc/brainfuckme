import javax.microedition.lcdui.*;

public class PantallaGuardarComo extends Form implements Pantalla, CommandListener {
	public final static int CMD_ACEPTAR = 0;
	public final static int CMD_CANCELAR = 1;
	protected int ultimoComando;
	
	protected final static Command cmdAceptar = new Command("Aceptar",Command.OK,1);
	protected final static Command cmdCancelar = new Command("Cancelar",Command.CANCEL,1);
	
	protected final static TextField nombre = new TextField("Nombre del programa","",128,TextField.ANY);

	public PantallaGuardarComo() {
		super("Guardar como...");
		
		ultimoComando = CMD_CANCELAR;
		
		append(nombre);
		
		addCommand(cmdAceptar);
		addCommand(cmdCancelar);
		setCommandListener(this);
		
	}
	
	public void commandAction(Command c, Displayable d) {
		if (c == cmdCancelar) {
			ultimoComando = CMD_CANCELAR;
			GestorPantallas.cerrarPantalla();
		} else if (c == cmdAceptar) {
			ultimoComando = CMD_ACEPTAR;
			GestorPantallas.cerrarPantalla();
		}
	}
	
	public void ponNombre(String nombre) {
		this.nombre.setString(nombre);
	}
	
	public String devNombre() {
		return nombre.getString();
	}
	
	public void alAbrirPantalla() {
	}
	
	public void alCerrarPantalla() {
	}

	public int devUltimoComando() {
		return ultimoComando;
	}
}