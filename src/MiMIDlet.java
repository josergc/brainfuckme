import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class MiMIDlet extends MIDlet {
	protected static MiMIDlet esteMIDlet;
	protected void startApp() throws 
		MIDletStateChangeException
		{
		esteMIDlet = this;
	}
	protected void pauseApp() {
	}
	protected void destroyApp(boolean unconditional) throws 
		MIDletStateChangeException
		{
	}
	public static void finalizarAplicacion() {
		try {
			esteMIDlet.destroyApp(false);
			esteMIDlet.notifyDestroyed();
		} catch (MIDletStateChangeException e) {
			GestorPantallas.abrirPantalla(
				new PantallaError("No se puede finalizar la aplicación",e.toString())
				);
		}
	}
}