import java.io.*;
import javax.microedition.lcdui.*;
import javax.microedition.rms.*;

public class PantallaSalida extends TextBox implements Pantalla, CommandListener {
	public final static int CMD_GUARDAR = 0;
	public final static int CMD_VOLVER = 1;
	protected int ultimoComando;

	protected final static Command cmdGuardar = new Command("Guardar como programa",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);

	public PantallaSalida() {
		super("Salida",null,32 * 1024,TextField.ANY);
		
		ultimoComando = CMD_VOLVER;
		
		addCommand(cmdGuardar);
		addCommand(cmdVolver);
		setCommandListener(this);
	}

	public void commandAction(Command c, Displayable d) {
		if (c == cmdVolver) {
			ultimoComando = CMD_VOLVER;
			GestorPantallas.cerrarPantalla();
		} else if (c == cmdGuardar) {
			ultimoComando = CMD_GUARDAR;
			GestorPantallas.abrirPantalla(GestorPantallas.pantallaGuardarComo);
		}
	}
	
	public void alAbrirPantalla() {
		if (ultimoComando == CMD_GUARDAR) {
			if (GestorPantallas.pantallaGuardarComo.devUltimoComando() == PantallaGuardarComo.CMD_ACEPTAR) {
				Programa p = new Programa();
				p.ponNombre(GestorPantallas.pantallaGuardarComo.devNombre());
				p.ponCodigo(getString());
				try {
					Programa.guarda(p);
					GestorPantallas.abrirPantalla(
						new PantallaMensaje("Nuevo programa guardado")
						);
				} catch(RecordStoreNotOpenException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Tabla no abierta",e.toString())
						);
				} catch(InvalidRecordIDException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("ID no v�lida",e.toString())
						);
				} catch(IOException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Error de entrada/salida",e.toString())
						);
				} catch(RecordStoreException e) {
					GestorPantallas.abrirPantalla(
						new PantallaError("Error en la tabla",e.toString())
						);
				}
				ultimoComando = CMD_VOLVER;
			}
		}
	}
	
	public void alCerrarPantalla() {
	}

}