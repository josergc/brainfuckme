import java.io.*;
import java.util.*;
import javax.microedition.rms.*;

public class Programa {
	protected static RecordStore rsProgramas;
	
	static {
		try {
			rsProgramas = RecordStore.openRecordStore("Brainfuck.Programa",true);
		} catch (RecordStoreNotFoundException e) {
		} catch (RecordStoreFullException e) {
		} catch (RecordStoreException e) {
		} catch (IllegalArgumentException e) {
		}
	}
	
	public static Enumeration devListaProgramas() throws
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		IOException,
		RecordStoreException
		{
		Vector nombres = new Vector();
		RecordEnumeration re = rsProgramas.enumerateRecords(null,null,false);
		
		while (re.hasNextElement()) 
			nombres.addElement(
				new DataInputStream(new ByteArrayInputStream(re.nextRecord())).readUTF()
				);
		
		return nombres.elements();
	}
	
	public static String devDescripcion(String nombre) throws
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		IOException,
		RecordStoreException
		{
		int id;
		RecordEnumeration re = rsProgramas.enumerateRecords(null,null,false);
		byte[] ba;
		DataInputStream dis;
		
		while (re.hasNextElement()) {
			id = re.nextRecordId();
			ba = rsProgramas.getRecord(id);
			if ((dis = new DataInputStream(
				new ByteArrayInputStream(ba)
				)).readUTF().compareTo(nombre) == 0)
				return dis.readUTF();
		}
		
		return null;
	}
	
	public static Programa carga(String nombre) throws
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		IOException,
		RecordStoreException
		{
		int id;
		RecordEnumeration re = rsProgramas.enumerateRecords(null,null,false);
		byte[] ba;
		
		while (re.hasNextElement()) {
			id = re.nextRecordId();
			ba = rsProgramas.getRecord(id);
			if (new DataInputStream(
				new ByteArrayInputStream(ba)
				).readUTF().compareTo(nombre) == 0)
				return new Programa(new ByteArrayInputStream(ba));
		}
		
		return null;
	}
	
	public static void guarda(Programa p) throws
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		IOException,
		RecordStoreException
		{
		int id;
		String nombre = p.devNombre();
		RecordEnumeration re = rsProgramas.enumerateRecords(null,null,false);
		byte[] ba;
		
		if (p.devRecordId() == 0)
			while (re.hasNextElement()) {
				id = re.nextRecordId();
				ba = rsProgramas.getRecord(id);
				if (new DataInputStream(
					new ByteArrayInputStream(ba)
					).readUTF().compareTo(nombre) == 0) {
						ba = p.aArrayDeBytes();
						rsProgramas.setRecord(id,ba,0,ba.length);
						p.ponRecordId(id);
						return;
					}
			}
		ba = p.aArrayDeBytes();
		p.ponRecordId(rsProgramas.addRecord(ba,0,ba.length));
	}
	
	public static boolean elimina(String nombre) throws
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		IOException,
		RecordStoreException
		{
		int id;
		RecordEnumeration re = rsProgramas.enumerateRecords(null,null,false);
		byte[] ba;
		
		while (re.hasNextElement()) {
			id = re.nextRecordId();
			ba = rsProgramas.getRecord(id);
			if (new DataInputStream(
				new ByteArrayInputStream(ba)
				).readUTF().compareTo(nombre) == 0) {
					rsProgramas.deleteRecord(id);
					return true;
				}
		}
		return false;
	}
	
	protected boolean modificado;
	protected int id;
	protected String nombre;
	protected String descripcion;
	protected String codigo;
	protected String entrada;
	
	public Programa() {
		modificado = false;
		id = -1;
		nombre = new String();
		descripcion = new String();
		codigo = new String();
		entrada = new String();
	}
	
	public Programa(InputStream is) throws
		IOException
		{
		DataInputStream dis = new DataInputStream(is);
		modificado = false;
		id = -1;
		nombre = dis.readUTF();
		descripcion = dis.readUTF();
		codigo = dis.readUTF();
		entrada = dis.readUTF();
	}
	
	public byte[] aArrayDeBytes() throws
		IOException
		{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		
		dos.writeUTF(nombre);
		dos.writeUTF(descripcion);
		dos.writeUTF(codigo);
		dos.writeUTF(entrada);
		
		modificado = false;
		
		return baos.toByteArray();
	}
	
	protected void ponRecordId(int id) {
		this.id = id;
	}
	
	protected int devRecordId() {
		return id;
	}
	
	public boolean haSidoModificado() {
		return modificado;
	}
	
	public void ponNombre(String nombre) {
		this.nombre = new String(nombre);
		id = 0;
	}

	public void ponDescripcion(String descripcion) {
		this.descripcion = new String(descripcion);
		modificado = true;
	}

	public void ponCodigo(String codigo) {
		this.codigo = new String(codigo);
		modificado = true;
	}
	
	public void ponEntrada(String entrada) {
		this.entrada = new String(entrada);
		modificado = true;
	}
	
	public String devNombre() {
		return new String(nombre);
	}
	
	public String devCodigo() {
		return new String(codigo);
	}
	
	public String devDescripcion() {
		return new String(descripcion);
	}
	
	public String devEntrada() {
		return new String(entrada);
	}
}