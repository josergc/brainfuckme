import javax.microedition.lcdui.*;

public class PantallaBrainfuck extends Form implements Pantalla, CommandListener {
	public PantallaBrainfuck() {
		super("Brainfuck");
		
		append(new StringItem("�Qu� es 'Brainfuck'?","'Brainfuck' es un lenguaje de programaci�n esot�rico Turing-completo."));
		append("Este lenguaje de programaci�n fu� creado por Urban M�ller en 1993 para hacer justamente lo contrario que se pretende con un lenguaje t�pico.");
		append("El objetivo de 'Brainfuck' es hacer la programaci�n dif�cil usando un conjunto reducido de instrucciones.");
		
		append(new StringItem("El int�rprete","Contiene un array de algo m�s de 30000 bytes iniciados a 0, un puntero que se desplaza dentro de ese array, un flujo de entrada y un flujo de salida."));
		
		append(new StringItem("Intrucciones","Ocho son las instrucciones de 'Brainfuck':"));
		append("> Incrementa el puntero");
		append("< Decrementa el puntero");
		append("+ Incrementa el byte apuntado por el puntero");
		append("- Decrementa el byte apuntado por el puntero");
		append(". Primer el byte en la salida");
		append(", Obtiene un byte de la entrada");
		append("[ Repetir mientras que el byte apuntado no sea 0");
		append("] Fin del bucle");
		
		addCommand(new Command("Volver",Command.EXIT,1));
		setCommandListener(this);
		
	}

	public void commandAction(Command c, Displayable d) {
		GestorPantallas.cerrarPantalla();
	}
	
	public void alAbrirPantalla() {
	}
	
	public void alCerrarPantalla() {
	}
}