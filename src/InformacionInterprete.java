/*

Informaci�n sobre la ejecuci�n del int�rprete de Brainfuck

Autor: Jos� Roberto Garc�a Chico
Versi�n: 1.0
Fecha: 17 de junio de 2006
Web: www.josergc.tk
e-mail: josergc@lycos.es

*/

public class InformacionInterprete {
	// Si no ha habido ning�n evento
	public final static int MSJ_CONTINUA = 0;
	// Si ha finalizado la ejecuci�n del programa
	public final static int MSJ_FINALIZADO = 1;
	// Si se necesitan datos en la entrada
	public final static int MSJ_NECESITA_ENTRADA = 2;
	// Si ha sacado un caracter por pantalla
	public final static int MSJ_SALIDA_DATOS = 3;
	// Si se ha producido un error sin identificar
	public final static int MSJ_ERROR = 4;
	// Si se ha llegado al final del programa buscando el s�mbolo de cerrar corchetes
	public final static int MSJ_ERROR_CORCHETE_SIN_CERRAR= 5;

	public int pc;
	public int mensaje;
	public int fila;
	public int columna;
	public InformacionInterprete() {
		pc = 0;
		mensaje = MSJ_CONTINUA;
		fila = 1;
		columna = 1;
	}
	public InformacionInterprete(int pc, int fila, int columna) {
		this.pc = pc;
		mensaje = MSJ_CONTINUA;
		this.fila = fila;
		this.columna = columna;
	}
	public InformacionInterprete(InformacionInterprete ii) {
		pc = ii.pc;
		mensaje = ii.mensaje;
		fila = ii.fila;
		columna = ii.columna;
	}
}